DISTRO = deepthirst

################################
# DeepTHIRST specific targets
################################

TALOS_FIRMWARE_BRANCH = v1.06
IMAGES = \
	talos-firmware-image

.PHONY: talos-firmware-image
talos-firmware-image:
	./bb-wrapper talos-firmware-image

.PHONY: talos-firmware-create-branch
talos-firmware-create-branch:
	git --git-dir=workspace/talos-hostboot/.git branch $(TALOS_FIRMWARE_BRANCH)
	git --git-dir=workspace/talos-skiboot/.git branch $(TALOS_FIRMWARE_BRANCH)
	git --git-dir=workspace/talos-petitboot/.git branch $(TALOS_FIRMWARE_BRANCH)
	git --git-dir=workspace/talos-occ/.git branch $(TALOS_FIRMWARE_BRANCH)
	git --git-dir=workspace/talos-hcode/.git branch $(TALOS_FIRMWARE_BRANCH)
